#
# Be sure to run `pod lib lint TCSSDK.podspec' to ensure this is a
# valid spec before submitting.
#
# Any lines starting with a # are optional, but their use is encouraged
# To learn more about a Podspec see https://guides.cocoapods.org/syntax/podspec.html
#

Pod::Spec.new do |s|
  s.name             = 'TCSSDK'
  s.version          = '0.1.3'
  s.summary          = 'A short description of TCSSDK.'

  s.homepage         = 'https://bitbucket.org/iagentur/tcssdkpod/src'
  s.license          = { :type => 'internal', :text => 'Copyright © 2019 iAgentur GmbH. All rights reserved.' }
  s.author           = { 'iAgentur GmbH' => 'vti@iagentur.ch' }
  s.source           = { :git => 'https://bitbucket.org/iagentur/tcssdkpod/src', :tag => s.version.to_s }

  s.ios.deployment_target = '10.0'
  s.source_files = 'TCSSDK/Classes/**/*'
  s.resources = 'TCSSDK/Assets/*'
  s.swift_version = '5.0'
  
  s.dependency 'Moya', '~> 13.0.1'
  s.dependency 'SWXMLHash', '~> 4.9.0'
  s.dependency 'Moya-ObjectMapper'
  s.dependency 'KeychainAccess', '~> 3.2.0'
end
